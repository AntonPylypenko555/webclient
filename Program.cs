using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryPark
{
    
    class Program
    {
        static HttpClient client = new HttpClient();

        static void ShowProduct(Product product)
        {
            Console.WriteLine($"Name: {product.Name}\tPrice: " +
                $"{product.Price}\tCategory: {product.Category}");
        }

        static async Task<Uri> CreateProductAsync(Product product)
        {
            HttpResponseMessage response = await client.GetAsync(
                "api/values");
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }

        static async Task<Product> GetProductAsync(string path)
        {
            Product product = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
              //  product = await response.Content.ReadAsAsync<Product>();
            }
            return product;
        }

        static async Task<Product> UpdateProductAsync(Product product)
        {
          //  HttpResponseMessage response = await client.PutAsJsonAsync(
           //     $"api/products/{product.Id}", product);
         //   response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
           // product = await response.Content.ReadAsAsync<Product>();
            return null;
        }

        static async Task<HttpStatusCode> DeleteProductAsync(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync(
                $"api/products/{id}");
            return response.StatusCode;
        }

        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("http://localhost:65219");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                Car car;

                var url = await CreateProductAsync(product);
              //  Console.WriteLine($"Created at {url}");

                // Get the product
                product = await GetProductAsync(url.PathAndQuery);
                ShowProduct(product);

                //// Update the product
                //Console.WriteLine("Updating price...");
                //product.Price = 80;
                //await UpdateProductAsync(product);

                //// Get the updated product
                //product = await GetProductAsync(url.PathAndQuery);
                //ShowProduct(product);

                //// Delete the product
                //var statusCode = await DeleteProductAsync(product.Id);
                //Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }

    //class Program
    //{

    // //   static HttpClient client = new HttpClient();


    //    static void Main(string[] args)
    //    {
    //        using (HttpClient client = new HttpClient())
    //        {
    //            using (HttpResponseMessage response =  client.GetAsync("http://localhost:65219"))
    //            {

    //            }
    //        }

    //            ParkLot parkLot = ParkLot.Initialize();


    //        bool flag = true;

    //        do
    //        {
    //            Console.WriteLine("   Binary studio parking lot!   ");
    //            Console.WriteLine("             Menu: ");
    //            Console.WriteLine("1. To put my car on a meter. ");
    //            Console.WriteLine("2. To get my car out. ");
    //            Console.WriteLine("3. Check your balance. ");
    //            Console.WriteLine("4. Recharge the balance. ");
    //            Console.WriteLine("5. Current balance of parklot. ");
    //            Console.WriteLine("6. Show transactions for the last minute. "); 
    //            Console.WriteLine("7. Show all transactions. ");       
    //            Console.WriteLine("8. The amount of free&busy places. ");
    //            Console.WriteLine("9. The whole list of transports. ");
    //            Console.WriteLine("0. Exit. ");

    //            string stringOfChoice = Console.ReadLine();

    //            // Checking of the input
    //            switch (stringOfChoice)
    //            {
    //                case "1":
    //                    chooseTheTypeOfCar();
    //                    break;
    //                case "2":
    //                    parkLot.GetCar(getTheCarNumber());
    //                    break;
    //                case "3":
    //                    parkLot.ShowTheBalanceOfCar(getTheCarNumber());
    //                    break;
    //                case "4":
    //                    parkLot.RechargeTheCarBalance(getTheCarNumber());
    //                    break;
    //                case "5":
    //                    Console.WriteLine("The balance of the parklot: " + parkLot.Balance);
    //                    break;
    //                case "6":
    //                    parkLot.ShowLastTransactions();
    //                    break;
    //                case "7":
    //                    parkLot.ShowAllTransactions();
    //                    break;
    //                case "8":
    //                    parkLot.ShowTheAmountOfFreeAndBusyLots();
    //                    break;
    //                case "9":
    //                    parkLot.ShowTheWholeListOfTransports();
    //                    break;
    //                case "0":
    //                    flag = false;
    //                    break;
    //                default:
    //                    Console.WriteLine("Please, type only 0 - 10 numbers.");
    //                    break;
    //            }                            
    //        } while (flag);


    //        void chooseTheTypeOfCar()
    //        {

    //            Console.WriteLine("\nPlease, choose your type of car.");
    //            Console.WriteLine("1. Passengercar; ");
    //            Console.WriteLine("2. Truck; ");
    //            Console.WriteLine("3. Bus; ");
    //            Console.WriteLine("4. Motocycle. ");

    //            string stringOfChoice = Console.ReadLine();

    //            switch (stringOfChoice)
    //            {
    //                case "1":
    //                    parkLot.AddCar(new Passengercar(getTheCarNumber()));
    //                    break;
    //                case "2":
    //                    parkLot.AddCar(new TruckCar(getTheCarNumber()));
    //                    break;
    //                case "3":
    //                    parkLot.AddCar(new Bus(getTheCarNumber()));
    //                    break;
    //                case "4":
    //                    parkLot.AddCar(new Motocycle(getTheCarNumber()));
    //                    break;
    //                case "0":
    //                    flag = false;
    //                    break;
    //                default:
    //                    Console.WriteLine("\nPlease, type only 0 - 4 numbers.");
    //                    break;
    //            }
    //        }

    //        string getTheCarNumber()
    //        {

    //            Console.WriteLine("\nPlease, write the registration number of your car");
    //            return Console.ReadLine();
    //        }
    //    }
//}
}
